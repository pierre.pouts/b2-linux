# TP1 : Premiers pas Docker

## Sommaire


# 0. Setup

# I. Init

## 1. Installation de Docker


## 2. Vérifier que Docker est bien là


## 3. sudo c pa bo

🌞 **Ajouter votre utilisateur au groupe `docker`**

```powershell
[baptiste@pierre ~]$ sudo gpasswd -a $USER docker

[baptiste@pierre ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```


## 4. Un premier conteneur en vif


🌞 **Lancer un conteneur NGINX**

```powershell
[baptiste@pierre ~]$ docker run -d -p 9999:80 nginx
Unable to find image 'nginx:latest' locally
latest: Pulling from library/nginx
af107e978371: Pull complete
336ba1f05c3e: Pull complete
8c37d2ff6efa: Pull complete
51d6357098de: Pull complete
782f1ecce57d: Pull complete
5e99d351b073: Pull complete
7b73345df136: Pull complete
Digest: sha256:bd30b8d47b230de52431cc71c5cce149b8d5d4c87c204902acf2504435d4b4c9
Status: Downloaded newer image for nginx:latest
a65a1bffabfd228042e31635c64bf860e7d8ce36fd5ff7c825edac4714b8ef45
```

🌞 **Visitons**

```powershell
[baptiste@pierre ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS         PORTS                                   NAMES
a65a1bffabfd   nginx     "/docker-entrypoint.…"   2 minutes ago   Up 2 minutes   0.0.0.0:9999->80/tcp, :::9999->80/tcp   charming_shtern

[baptiste@pierre ~]$ docker logs charming_shtern
/docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
/docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
/docker-entrypoint.sh: Launching /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh
10-listen-on-ipv6-by-default.sh: info: Getting the checksum of /etc/nginx/conf.d/default.conf
10-listen-on-ipv6-by-default.sh: info: Enabled listen on IPv6 in /etc/nginx/conf.d/default.conf
/docker-entrypoint.sh: Sourcing /docker-entrypoint.d/15-local-resolvers.envsh
/docker-entrypoint.sh: Launching /docker-entrypoint.d/20-envsubst-on-templates.sh
/docker-entrypoint.sh: Launching /docker-entrypoint.d/30-tune-worker-processes.sh
/docker-entrypoint.sh: Configuration complete; ready for start up
2023/12/21 15:06:22 [notice] 1#1: using the "epoll" event method
2023/12/21 15:06:22 [notice] 1#1: nginx/1.25.3
2023/12/21 15:06:22 [notice] 1#1: built by gcc 12.2.0 (Debian 12.2.0-14)
2023/12/21 15:06:22 [notice] 1#1: OS: Linux 5.14.0-362.13.1.el9_3.x86_64
2023/12/21 15:06:22 [notice] 1#1: getrlimit(RLIMIT_NOFILE): 1073741816:1073741816
2023/12/21 15:06:22 [notice] 1#1: start worker processes
2023/12/21 15:06:22 [notice] 1#1: start worker process 28

[baptiste@pierre ~]$ docker inspect charming_shtern
[
    {
        "Id": "a65a1bffabfd228042e31635c64bf860e7d8ce36fd5ff7c825edac4714b8ef45",
        "Created": "2023-12-21T15:06:22.133693746Z",
        "Path": "/docker-entrypoint.sh",
        "Args": [
            "nginx",
            "-g",
            "daemon off;"
        ],
        "State": {
            "Status": "running",
            "Running": true,
            "Paused": false,
            "Restarting": false,
            "OOMKilled": false,
            "Dead": false,
            "Pid": 6780,
            "ExitCode": 0,
            "Error": "",
            "StartedAt": "2023-12-21T15:06:22.761632664Z",
            "FinishedAt": "0001-01-01T00:00:00Z"
        },
        "Image": "sha256:d453dd892d9357f3559b967478ae9cbc417b52de66b53142f6c16c8a275486b9",
        "ResolvConfPath": "/var/lib/docker/containers/a65a1bffabfd228042e31635c64bf860e7d8ce36fd5ff7c825edac4714b8ef45/resolv.conf",
        "HostnamePath": "/var/lib/docker/containers/a65a1bffabfd228042e31635c64bf860e7d8ce36fd5ff7c825edac4714b8ef45/hostname",
        "HostsPath": "/var/lib/docker/containers/a65a1bffabfd228042e31635c64bf860e7d8ce36fd5ff7c825edac4714b8ef45/hosts",
        "LogPath": "/var/lib/docker/containers/a65a1bffabfd228042e31635c64bf860e7d8ce36fd5ff7c825edac4714b8ef45/a65a1bffabfd228042e31635c64bf860e7d8ce36fd5ff7c825edac4714b8ef45-json.log",
        "Name": "/charming_shtern",
        "RestartCount": 0,
        "Driver": "overlay2",
        "Platform": "linux",
        "MountLabel": "",
        "ProcessLabel": "",
        "AppArmorProfile": "",
        "ExecIDs": null,
        "HostConfig": {
            "Binds": null,
            "ContainerIDFile": "",
            "LogConfig": {
                "Type": "json-file",
                "Config": {}
            }
        }
    }   
] 

[baptiste@pierre ~]$ sudo ss -lnpt
State      Recv-Q     Send-Q         Local Address:Port          Peer Address:Port     Process
LISTEN     0          4096                 0.0.0.0:9999               0.0.0.0:*         users:(("docker-proxy",pid=6740,fd=4))

[baptiste@pierre ~]$ sudo firewall-cmd --add-port=9999/tcp --permanent
success
[baptiste@pierre ~]$ sudo firewall-cmd --reload
success

[baptiste@pierre ~]$ curl 10.1.1.13:9999
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
```

🌞 **On va ajouter un site Web au conteneur NGINX**

```powershell
[baptiste@pierre nginx]$ docker stop a6

[baptiste@localhost nginx]$ cat index.html
<h1>MEOOOW<h1>

[baptiste@localhost nginx]$ cat site_nul.conf
server {
    listen        8080;

    location / {
        root /var/www/html;
    }
}

```


🌞 **Visitons**

```powershell
[baptiste@pierre nginx]$ curl 10.1.1.13:9999
<h1>MEOOOW<h1>

[baptiste@pierre nginx]$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS         PORTS                                               NAMES
6a7ae0247ae2   nginx     "/docker-entrypoint.…"   9 minutes ago   Up 9 minutes   80/tcp, 0.0.0.0:9999->8080/tcp, :::9999->8080/tcp   reverent_cohen

```


## 5. Un deuxième conteneur en vif


🌞 **Lance un conteneur Python, avec un shell**

```powershell
[baptiste@pierre ~]$ docker run -it python bash
Unable to find image 'python:latest' locally
latest: Pulling from library/python
bc0734b949dc: Pull complete
b5de22c0f5cd: Pull complete
917ee5330e73: Pull complete
b43bd898d5fb: Pull complete
7fad4bffde24: Pull complete
d685eb68699f: Pull complete
107007f161d0: Pull complete
02b85463d724: Pull complete
Digest: sha256:3733015cdd1bd7d9a0b9fe21a925b608de82131aa4f3d397e465a1fcb545d36f
Status: Downloaded newer image for python:latest
root@2f51e78db6e7:/#
```

🌞 **Installe des libs Python**

```powershell
root@2f51e78db6e7:/# pip install aiohttp

Successfully installed aiohttp-3.9.1 aiosignal-1.3.1 attrs-23.1.0 frozenlist-1.4.1 idna-3.6 multidict-6.0.4 yarl-1.9.4

root@2f51e78db6e7:/# pip install aioconsole
Successfully installed aioconsole-0.7.0
```

# II. Images


## 1. Images publiques

🌞 **Récupérez des images**

```powershell
[baptiste@localhost ~]$ docker pull python:3.11

[baptiste@localhost ~]$ docker pull mysql:5.7

[baptiste@localhost ~]$ docker pull wordpress

[baptiste@localhost ~]$ docker pull linuxserver/wikijs

[baptiste@pierre ~]$ docker image ls
REPOSITORY           TAG       IMAGE ID       CREATED        SIZE
linuxserver/wikijs   latest    869729f6d3c5   6 days ago     441MB
mysql                5.7       5107333e08a8   9 days ago     501MB
python               latest    fc7a60e86bae   2 weeks ago    1.02GB
wordpress            latest    fd2f5a0c6fba   2 weeks ago    739MB
```

🌞 **Lancez un conteneur à partir de l'image Python**

```powershell
[baptiste@pierre ~]$ docker run -it python bash
```


## 2. Construire une image


🌞 **Ecrire un Dockerfile pour une image qui héberge une application Python**

```powershell
[baptiste@pierre test_build]$ cat Dockerfile
FROM debian:latest

RUN apt update -y && apt install -y python3 && apt install -y python3-pip && apt install python3-emoji

COPY app.py /app.py

ENTRYPOINT ["python3", "/app.py"]
```

🌞 **Build l'image**

```powershell
[baptiste@pierre test_build]$ docker build -t python_emoji_app .
```


🌞 **Lancer l'image**

```powershell
[baptiste@pierre test_build]$ docker run python_emoji_app
Cet exemple d'application est vraiment naze 👎
```

# III. Docker compose



🌞 **Créez un fichier `docker-compose.yml`**

```powershell
[baptiste@pierre compose_test]$ mkdir compose_test
[baptiste@pierre compose_test]$ nano docker-compose.yml
[baptiste@pierre compose_test]$ cat docker-compose.yml
version: "3"

services:
  conteneur_nul:
    image: debian
    entrypoint: sleep 9999
  conteneur_flopesque:
    image: debian
    entrypoint: sleep 9999
```

🌞 **Lancez les deux conteneurs** avec `docker compose`

```powershell
[baptiste@pierre compose_test]$ docker compose up -d
[+] Running 3/3
 ✔ conteneur_flopesque 1 layers [⣿]      0B/0B      Pulled                                                                                                                    8.7s
   ✔ bc0734b949dc Already exists                                                                                                                                              0.0s
 ✔ conteneur_nul Pulled                                                                                                                                                       8.7s
[+] Running 3/3
 ✔ Network compose_test_default                  Created                                                                                                                      0.3s
 ✔ Container compose_test-conteneur_flopesque-1  Started                                                                                                                      0.0s
 ✔ Container compose_test-conteneur_nul-1        Started
```


🌞 **Vérifier que les deux conteneurs tournent**

```powershell
[baptiste@pierre compose_test]$ docker compose ps
NAME                                 IMAGE     COMMAND        SERVICE               CREATED              STATUS              PORTS
compose_test-conteneur_flopesque-1   debian    "sleep 9999"   conteneur_flopesque   About a minute ago   Up About a minute
compose_test-conteneur_nul-1         debian    "sleep 9999"   conteneur_nul         About a minute ago   Up About a minute
```


🌞 **Pop un shell dans le conteneur `conteneur_nul`**

```powershell
[baptiste@pierre compose_test]$ docker exec -it compose_test-conteneur_nul-1 bash
root@2519c761b325:/# apt update
root@2519c761b325:/# apt install iputils-ping -y
root@2519c761b325:/# ping conteneur_flopesque
PING conteneur_flopesque (172.18.0.2) 56(84) bytes of data.
64 bytes from compose_test-conteneur_flopesque-1.compose_test_default (172.18.0.2): icmp_seq=1 ttl=64 time=0.100 ms
64 bytes from compose_test-conteneur_flopesque-1.compose_test_default (172.18.0.2): icmp_seq=2 ttl=64 time=0.093 ms
64 bytes from compose_test-conteneur_flopesque-1.compose_test_default (172.18.0.2): icmp_seq=3 ttl=64 time=0.126 ms
64 bytes from compose_test-conteneur_flopesque-1.compose_test_default (172.18.0.2): icmp_seq=4 ttl=64 time=0.081 ms
^C
--- conteneur_flopesque ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 2998ms
rtt min/avg/max/mdev = 0.081/0.100/0.126/0.016 ms
```




demander a leo 

[baptiste@pierre compose_test]$ docker compose up -d
[+] Running 2/2
 ✘ conteneur_nul Error                                                       20.0s
 ✘ conteneur_flopesque Error                                                 20.0s
Error response from daemon: Get "https://registry-1.docker.io/v2/": dial tcp: lookup registry-1.docker.io on 172.20.10.1:53: read udp 10.0.3.15:52368->172.20.10.1:53: i/o timeout